import React, { Component } from 'react';

import Banner from './Banner/';
import BelongingsList from './BelongingsList/';

class App extends Component {
  render() {
    return (
      <div className="app">
        <Banner />
        <BelongingsList />
      </div>
    );
  }
}

export default App;
