import React, { Component } from 'react';

import Category from '../Category';

import belongings from '../belongings.json';

const categories = [...new Set(belongings.map(b => b.category))];

class BelongingsList extends Component {
  constructor() {
    super();

    this.state = {
      category: 0
    };

    this.handleCategoryChange = this.handleCategoryChange.bind(this);
  }

  handleCategoryChange(direction) {
    const index = this.state.category;

    const increment = direction === 'prev' ? -1 : 1;
    let nextIndex = index + increment;

    if (nextIndex < 0) nextIndex = categories.length - 1;
    if (nextIndex === categories.length) nextIndex = 0;

    this.setState({
      category: nextIndex
    });
  }

  render() {
    const { category } = this.state;
    const categoryName = categories[category];

    const $belongings = belongings
      .filter(b => b.category === categoryName)
      .map((b, i) => <Belonging key={i} image={b.image} name={b.name} />);

    return (
      <div className="belongings">
        <Category name={categoryName} onChange={this.handleCategoryChange} />
        <div className="belongings__categories" />
        <ul className="belongings__list">{$belongings}</ul>;
      </div>
    );
  }
}

class Belonging extends Component {
  render() {
    const { image, name } = this.props;
    const publicUrl = process.env.PUBLIC_URL;

    return (
      <li className="belonging">
        <div className="belonging__image">
          <img
            alt={name}
            sizes={`
              (min-width: 600px) 425px,
              100vw
            `}
            srcSet={`
              ${publicUrl}/belongings/${image}-480.jpg 480w,
              ${publicUrl}/belongings/${image}-1024.jpg 1024w
            `}
            src={`${publicUrl}/belongings/${image}-480.jpg`}
          />
        </div>
        <div className="belonging__body">
          <div className="belonging__name">{name}</div>
        </div>
      </li>
    );
  }
}

export default BelongingsList;
