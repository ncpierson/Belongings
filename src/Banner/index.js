import React, { Component } from 'react';

import belongings from '../belongings.json';

class Banner extends Component {
  constructor() {
    super();

    this.handleArrowClick = this.handleArrowClick.bind(this);
  }

  handleArrowClick(e) {
    const list = document.querySelector('.belongings');
    list.scrollIntoView({ behavior: 'smooth', block: 'start' });
  }

  render() {
    const itemsOwned = belongings.length;

    return (
      <div className="banner">
        <div className="banner__content">
          <span>{itemsOwned}</span>
          <h1 className="banner__label">Belongings</h1>
        </div>
        <div className="banner__arrow" onClick={this.handleArrowClick} />
      </div>
    );
  }
}

export default Banner;
