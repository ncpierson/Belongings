import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import './index.css';
import './styles/banner.css';
import './styles/belongings.css';
import './styles/category.css';

ReactDOM.render(<App />, document.getElementById('root'));
