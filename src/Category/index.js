import React, { Component } from 'react';

class Category extends Component {
  render() {
    const { name, onChange } = this.props;

    return (
      <div className="category">
        <h2 className="category__name">
          <span
            className="category__arrow category__arrow--prev"
            onClick={() => onChange('prev')}
          />
          <span>{name}</span>
          <span
            className="category__arrow category__arrow--next"
            onClick={() => onChange('next')}
          />
        </h2>
      </div>
    );
  }
}

export default Category;
