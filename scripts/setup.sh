#! /bin/bash
# Run this on host server to install dependencies, setup nginx

# Must run as sudo, must be able to access username of user

if [ "$EUID" -eq 0 ]; then
  echo "Must not run as sudo!"
  exit 1
fi

# Software Dependencies

sudo apt-get update

sudo apt-get install nginx -y

## NVM / Node

curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.6/install.sh | bash
source ~/.nvm/nvm.sh
nvm install --lts

## Node Dependencies

npm install -g yarn

# Firewall Setup (DigitalOcean Specific)

sudo ufw allow OpenSSH
sudo ufw allow 'Nginx Full'
sudo ufw enable

# Deployment Setup

cd ~

[[ -d Deployment ]] || mkdir Deployment

sudo mkdir -p /var/www/belongings.nick.exposed
sudo chown $USER:$USER /var/www/belongings.nick.exposed

sudo bash -c "cat > /etc/nginx/sites-available/belongings.nick.exposed" <<"EOF"
map $sent_http_content_type $expires {
  default                off;
  text/html              epoch;
  text/css               max;
  application/javascript max;
  ~image/                max;
}

server {
  listen 80;
  listen [::]:80;

  root /var/www/belongings.nick.exposed;

  expires $expires;

  server_name belongings.nick.exposed;

  location / {
    try_files $uri /index.html;
  }
}
EOF

sudo ln -s /etc/nginx/sites-available/belongings.nick.exposed /etc/nginx/sites-enabled/
sudo service nginx reload
