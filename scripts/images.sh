#! /bin/bash

# Handle PNG images

# for PHOTO in assets/belongings/*.png; do
#   NAME=`basename $PHOTO .png`;
#   convert "$PHOTO" -strip -resize 480x -sharpen 0x.5 "src/BelongingsList/images/${NAME}-480.png"
#   convert "$PHOTO" -strip -resize 960x -sharpen 0x.5 "src/BelongingsList/images/${NAME}-960.png"
#   convert "$PHOTO" -strip -resize 1920x              "src/BelongingsList/images/${NAME}-1920.png"
# done

# pngquant -f --ext .png src/BelongingsList/images/*.png

# Handle JPG images

for PHOTO in assets/belongings/*.jpg; do
  NAME=`basename $PHOTO .jpg`;
  convert "$PHOTO" -sampling-factor 4:2:0 -strip -resize 480x -sharpen 0x.5 -quality 75 "public/belongings/${NAME}-480.jpg"
  convert "$PHOTO" -sampling-factor 4:2:0 -strip -resize 1024x -quality 75              "public/belongings/${NAME}-1024.jpg"
done
