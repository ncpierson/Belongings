#! /bin/bash

source ~/.nvm/nvm.sh

cd ~/Deployment/

rm -rf Belongings

git clone git@gitlab.com:ncpierson/Belongings.git

cd Belongings

yarn

yarn images

yarn build

cp -R build/* /var/www/belongings.nick.exposed
